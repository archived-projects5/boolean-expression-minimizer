import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class BooleanUtils {

    private static List<Boolean> bitSetToList(BitSet bs, int width) {
        ArrayList<Boolean> result = new ArrayList<>();
        for(int i = 0; i < width; i++) result.add(false);
        bs.stream().forEach(i -> result.set(width - i - 1, true));
        return result;
    }

    public static List<Boolean> getPermutation(int i, int width) {
        BitSet bitset = BitSet.valueOf(new long[]{i});
        return bitSetToList(bitset, width);
    }

}