import java.io.IOException;
import java.net.URISyntaxException;

public class EspressoTest extends MinimizationTest {

    @Override
    protected TruthTableData minimize(TruthTableData inputTruthTable, boolean minimizationTarget)
            throws IOException, URISyntaxException, InterruptedException {
        return new Espresso().apply(inputTruthTable, minimizationTarget);
    }
}