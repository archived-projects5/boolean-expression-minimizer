public class QuineMcCluskeyTest extends MinimizationTest {

    @Override
    protected TruthTableData minimize(TruthTableData inputTruthTable, boolean minimizationTarget)
            throws InterruptedException {
        return new QuineMcCluskey().apply(inputTruthTable, minimizationTarget);
    }
}