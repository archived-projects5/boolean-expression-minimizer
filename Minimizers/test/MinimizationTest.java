import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


public abstract class MinimizationTest {

    protected abstract TruthTableData minimize(
            TruthTableData inputTruthTable,
            boolean minimizationTarget) throws IOException, URISyntaxException, InterruptedException;


    @Test
    public void runFileTests() throws IOException, URISyntaxException, InterruptedException {
        for(File inputFile : getTestFiles()) {
            MinimizerTestInputData testInputData = parseFileData(inputFile);
            assertTrue(runTest(
                    testInputData.inputTruthTable,
                    testInputData.minimizationTarget
            ));
        }
    }

    @Test
    public void testForTwoInputVariables() throws IOException, URISyntaxException, InterruptedException {

        TruthTableData inputTruthTable = new TruthTableData();

        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._0, TriState._0), TriState._0));
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._0, TriState._1), TriState._1));
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._1, TriState._0), TriState._1));
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._1, TriState._1), TriState._1));

        TruthTableData outputTruthTableForTargetOne = minimize(inputTruthTable, true);

        assertEquals(2, outputTruthTableForTargetOne.size());

        assertTrue(outputTruthTableForTargetOne.stream().allMatch(row -> row.getOutput() == TriState._1));

        assertTrue(outputTruthTableForTargetOne.stream().anyMatch(row -> row.getInputs().containsAll(
                Arrays.asList(TriState._1, TriState._X)
        )));

        assertTrue(outputTruthTableForTargetOne.stream().anyMatch(row -> row.getInputs().containsAll(
                Arrays.asList(TriState._1, TriState._X)
        )));


        TruthTableData outputTruthTableForTargetZero = minimize(inputTruthTable, false);

        assertEquals(1, outputTruthTableForTargetZero.size());
        assertEquals(TriState._0, outputTruthTableForTargetZero.get(0).getOutput());
        assertTrue(outputTruthTableForTargetZero.get(0).getInputs().containsAll(
                Arrays.asList(TriState._0, TriState._0)
        ));

    }

    @Test
    public void testWhenFunctionIsAlwaysTrue() throws IOException, URISyntaxException, InterruptedException {
        TruthTableData inputTruthTable = new TruthTableData();
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._0), TriState._1));
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._1), TriState._1));

        assertTrue(runTest(inputTruthTable, true));
        assertTrue(runTest(inputTruthTable, false));
    }

    @Test
    public void testWhenFunctionIsAlwaysFalse() throws IOException, URISyntaxException, InterruptedException {
        TruthTableData inputTruthTable = new TruthTableData();
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._0), TriState._0));
        inputTruthTable.add(new TruthTableRow(Arrays.asList(TriState._1), TriState._0));

        assertTrue(runTest(inputTruthTable, false));
        assertTrue(runTest(inputTruthTable, true));
    }

    protected List<File> getTestFiles() {
        File testDataDir = new File("test/testdata");
        if (!testDataDir.isDirectory()) {
            throw new RuntimeException("Test data directory does not exist: " + testDataDir.getPath());
        }

        List<String> testFiles = Arrays.asList(testDataDir.list());
        if (testFiles.isEmpty()) {
            throw new RuntimeException("Test data directory is empty: " + testDataDir.getPath());
        }

        return testFiles
                .stream()
                .map(f -> new File(testDataDir, f))
                .collect(Collectors.toList());
    }

    protected boolean runTest(TruthTableData inputTruthTable, boolean minimizationTarget)
            throws IOException, URISyntaxException, InterruptedException {

        for(int i = 0; i < 5; i ++) {
            TruthTableData outputTruthTable = minimize(inputTruthTable, minimizationTarget);

            boolean outputsOk = outputTruthTable.stream().allMatch(row -> {
                return row.getOutput().toBoolean() == minimizationTarget;
            });

            boolean inputsOk = inputTruthTable.stream().allMatch(row -> {
                return checkRowInTable(row, outputTruthTable)
                    == (row.getOutput().toBoolean() == minimizationTarget);
            });

            boolean passed = outputsOk && inputsOk;
            if (passed) {
                return true;
            }
        }
        return false;
    }


    protected MinimizerTestInputData parseFileData(File file) throws IOException {

        TruthTableData inputData = new TruthTableData();
        Boolean minimizationTarget;

        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String line = reader.readLine();
            if (line == null || !line.startsWith("target ")) {
                throw new RuntimeException("Invalid test data format");
            }

            char minimizationTargetChar = line.charAt("target ".length());
            if (minimizationTargetChar != '0' && minimizationTargetChar != '1') {
                throw new RuntimeException("Invalid test data format");
            }

            minimizationTarget = minimizationTargetChar == '1';

            while((line = reader.readLine()) != null) {

                List<TriState> inputs = new ArrayList<>();
                for(int i = 0; i < line.length() - 2; i++) {
                    inputs.add(TriState.fromChar(line.charAt(i)));
                }

                TriState output = TriState.fromChar(line.charAt(line.length() - 1));

                inputData.add(new TruthTableRow(inputs, output));
            }
        }

        return new MinimizerTestInputData(minimizationTarget.booleanValue(), inputData);
    }

    private boolean checkRowInTable(TruthTableRow row, TruthTableData table) {
        for(TruthTableRow tableRow : table) {
            if (tableRow.getOutput() != row.getOutput()) {
                continue;
            }

            boolean allInputsMatch = true;
            for(int i = 0; i < table.inputCount(); i++) {
                TriState tableRowBit = tableRow.getInputs().get(i);
                if (tableRowBit != TriState._X && tableRowBit != row.getInputs().get(i)) {
                    allInputsMatch = false;
                    break;
                }
            }
            if (allInputsMatch) {
                return true;
            }
        }
        return false;
    }
}
