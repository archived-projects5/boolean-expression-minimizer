public class MinimizerTestInputData {

    public MinimizerTestInputData(
            boolean minimizationTarget,
            TruthTableData inputData) {

        this.minimizationTarget = minimizationTarget;
        this.inputTruthTable = inputData;
    }

    public final Boolean minimizationTarget;
    public final TruthTableData inputTruthTable;
}
