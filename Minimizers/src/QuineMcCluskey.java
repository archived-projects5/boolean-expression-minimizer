import java.util.*;
import java.util.stream.Collectors;

public class QuineMcCluskey {
    static class Implicant {

        public Implicant(String mask, int rowIndex) {
            this.mask = mask;
            this.rowIndices = new ArrayList<>();
            this.rowIndices.add(rowIndex);
        }

        public Implicant(String mask, List<Integer> rowIndices) {
            this.mask = mask;
            this.rowIndices = rowIndices;
        }

        public boolean isPrime() {
            return isPrime;
        }

        public String getMask() {
            return mask;
        }

        public int getLiteralsCount() {
            int count = 0;
            for(int i = 0; i < getMask().length(); i++) {
                if (getMask().charAt(i) != '-') {
                    count++;
                }
            }
            return count;
        }

        public List<Integer> getRowIndices() {
            return rowIndices;
        }

        private int findTheOnlyDifferingIndex(Implicant other) {
            int differingIndex = -1;
            for(int i = 0; i < getMask().length(); i++) {
                if (getMask().charAt(i) != other.getMask().charAt(i)) {
                    if (differingIndex == -1) {
                        differingIndex = i;
                    } else {
                        return -1;
                    }
                }
            }
            return differingIndex;
        }

        private String combineMask(String otherMask, int differingIndex) {
            StringBuilder combinedMaskBuilder = new StringBuilder(otherMask);
            combinedMaskBuilder.setCharAt(differingIndex, '-');
            return combinedMaskBuilder.toString();
        }

        private List<Integer> combineIndices(List<Integer> otherIndices) {
            ArrayList<Integer> combined = new ArrayList<>(getRowIndices());
            combined.addAll(otherIndices);
            return combined;
        }

        public Optional<Implicant> tryCombine(Implicant other) {
            int differingIndex = findTheOnlyDifferingIndex(other);
            if (differingIndex == -1) {
                return Optional.empty();
            }

            this.isPrime = false;
            other.isPrime = false;

            return Optional.of(new Implicant(
                    combineMask(other.getMask(), differingIndex),
                    combineIndices(other.getRowIndices())
            ));
        }
        private boolean isPrime = true;
        private final String mask;
        private final List<Integer> rowIndices;
    }

    static class ImplicantSet extends TreeSet<Implicant> {
        public ImplicantSet() {
            super(Comparator.comparing(i -> i.mask));
        }
    }

    class ImplicantTable {
        public ImplicantTable(int inputCount) {
            data = new ArrayList<>();
            for(int i = 0; i < inputCount + 1; i++) {
                data.add(new ArrayList<>());
                for(int j = 0; j < inputCount + 1 - i; j++) {
                    data.get(i).add(new ImplicantSet());
                }
            }
        }

        PrimeImplicantTable toPrimeImplicantTable() {
            HashMap<Integer, List<Implicant>> indexToImplicantsMap = new HashMap<Integer, List<Implicant>>();
            for(List<ImplicantSet> row : data) {
                for(ImplicantSet implicantSet : row) {
                    for(Implicant implicant : implicantSet) {
                        if (implicant.isPrime()) {
                            for(Integer index : implicant.getRowIndices()) {
                                if (indexToImplicantsMap.get(index) == null) {
                                    indexToImplicantsMap.put(index, new ArrayList<>());
                                }
                                indexToImplicantsMap.get(index).add(implicant);
                            }
                        }
                    }
                }
            }
            return new PrimeImplicantTable(indexToImplicantsMap);
        }

        public int dim() {
            return data.size();
        }

        public ImplicantSet get(int implicantLevel, int highInputCount) {
//            assert implicantLevel <= highInputCount;
            return data.get(implicantLevel).get(highInputCount);
        }

        private final List<List<ImplicantSet>> data;
    }

    private String toBinaryString(int number, int bitCount) {
        StringBuilder sb = new StringBuilder(Integer.toBinaryString(number));
        assert sb.length() <= bitCount;
        int unpaddedMaskLength = sb.length();
        for(int i = 0; i < bitCount - unpaddedMaskLength; i++) {
            sb.insert(0, '0');
        }
        return sb.toString();
    }

    private void createFirstLevelOfImplicants(
            ImplicantTable implicantTable,
            List<TruthTableRow> outputMatchingRows,
            boolean outputValue) {

        int i = 0;
        for(TruthTableRow row : outputMatchingRows) {
            StringBuilder sb = new StringBuilder();
            row.getInputs().stream().map(TriState::toChar).forEach(sb::append);
            String implicantMask = sb.toString();
            Implicant initialImplicant = new Implicant(implicantMask, i);
            int hiloBitCount = countOccurences(row, outputValue);
            implicantTable.get(0, hiloBitCount).add(initialImplicant);
            i++;
        }
    }

    private int countOccurences(TruthTableRow row, boolean value) {
        return (int) row.getInputs().stream().filter(inp -> inp == TriState.fromBoolean(value)).count();
    }

    private void combineTwoImplicantSets(
            ImplicantSet first, ImplicantSet second, ImplicantSet output) {
        for(Implicant currImp : first) {
            for(Implicant nextImp : second) {
                Optional<Implicant> combinedImp = currImp.tryCombine(nextImp);
                if (combinedImp.isPresent()) {
                    output.add(combinedImp.get());
                }
            }
        }
    }

    private void combineImplicantTableLevel(
            ImplicantTable implicantTable, int level) throws InterruptedException {
        for(int j = 0; j < implicantTable.dim() - level - 1; j++) {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            combineTwoImplicantSets(
                    implicantTable.get(level, j),
                    implicantTable.get(level, j+1),
                    implicantTable.get(level+1, j));
        }
    }

    private void fullyCombineImplicants(ImplicantTable implicantTable) throws InterruptedException {
        for(int i = 0; i < implicantTable.dim(); i++) {
            combineImplicantTableLevel(implicantTable, i);
        }
    }

    static class PrimeImplicantTable {

        static class EssentialAndRegularImplicants {
            public Set<Implicant> essentials = new HashSet<>();
            public List<List<Implicant>> regulars = new ArrayList<>();
        }

        public void print() {
            HashSet<Implicant> set = new HashSet<Implicant>();
            for(Map.Entry<Integer, List<Implicant>> kvp : indexToImplicantsMap.entrySet()) {
                set.addAll(kvp.getValue());
            }
        }

        public PrimeImplicantTable(Map<Integer, List<Implicant>> indexToImplicantsMap) {
            this.indexToImplicantsMap = indexToImplicantsMap;
        }

        public EssentialAndRegularImplicants splitToEssentialAndRegularImplicants() {
            EssentialAndRegularImplicants result = new EssentialAndRegularImplicants();

            HashSet<Integer> indicesAlreadyCoveredByEssentials = new HashSet<Integer>();

            indexToImplicantsMap
                    .entrySet()
                    .stream()
                    .filter(kvp -> kvp.getValue().size() == 1)
                    .forEach(indexAndImplicantList -> {
                        Implicant implicant = indexAndImplicantList.getValue().get(0);
                        indicesAlreadyCoveredByEssentials.addAll(implicant.getRowIndices());
                        result.essentials.add(implicant);
                    });

            indexToImplicantsMap
                    .entrySet()
                    .forEach(kvp -> {
                        if (!indicesAlreadyCoveredByEssentials.contains(kvp.getKey())) {
                            result.regulars.add(kvp.getValue());
                        }
                    });

            return result;
        }

        private final Map<Integer, List<Implicant>> indexToImplicantsMap;
    }

    private TruthTableData makeTruthTable(Collection<Implicant> implicants, boolean outputValue) {
        TruthTableData result = new TruthTableData();

        implicants.stream()
                .sorted(Comparator.comparing(i -> i.getMask()))
                .forEach(implicant -> {

                    List<TriState> inputs = implicant.getMask().chars()
                    .mapToObj(TriState::fromChar)
                    .collect(Collectors.toList());

                    TriState output = TriState.fromBoolean(outputValue);
            result.add(new TruthTableRow(inputs, output));

        });

        return result;
    }

    private List<ImplicantSum> makeImplicantsPoS(List<List<Implicant>> implicantGroups) {
        return implicantGroups.stream().map(group -> {

            Set<ImplicantProduct> basicProducts = group.stream()
                    .map(ImplicantProduct::new)
                    .collect(Collectors.toSet());

            return new ImplicantSum(basicProducts);

        }).collect(Collectors.toList());
    }

    private ImplicantSum simplifyToSoP(List<ImplicantSum> productOfSums) throws InterruptedException {
        ArrayList<ImplicantSum> posCopy = new ArrayList<>(productOfSums);

        while(posCopy.size() > 1) {
            ImplicantSum netSum = posCopy.get(0).mul(posCopy.get(1));
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            posCopy.remove(0);
            posCopy.set(0, netSum);
        }

        return posCopy.get(0);
    }

    private List<ImplicantProduct> getShortestProducts(ImplicantSum sumOfProducts) {

        int minProdSize = sumOfProducts
                .getProducts()
                .stream()
                .min(Comparator.comparing(ImplicantProduct::size))
                .get()
                .size();

        return sumOfProducts
                .getProducts()
                .stream()
                .filter(p -> p.size() == minProdSize)
                .collect(Collectors.toList());
    }

    private ImplicantProduct getFirstProductWithLeastLiterals(List<ImplicantProduct> products) {
        ImplicantProduct prodWithLeastImplicants = products.stream()
                .min(Comparator.comparing(ip -> ip.getImplicants().size()))
                .get();

        int leastImplicants = prodWithLeastImplicants.size();

        return products.stream()
                .filter(p -> p.getImplicants().size() == leastImplicants)
                .min(Comparator.comparing(ImplicantProduct::getLiteralsCount))
                .get();
    }

    private Set<Implicant> runPetricksMethod(List<List<Implicant>> implicantGroups) throws InterruptedException {

        List<ImplicantSum> implicantPoS =
                makeImplicantsPoS(implicantGroups);

        ImplicantSum implicantSoP =
                simplifyToSoP(implicantPoS);


//        for(ImplicantProduct p : implicantSoP.getProducts()) {
//            for(Implicant imp : p.getImplicants()) {
//                System.out.print(imp.getRowIndices() + " ");
//            }
//            System.out.println();
//        }

        List<ImplicantProduct> shortestProducts = getShortestProducts(implicantSoP);

        //System.out.println("Shortest products");
        for(ImplicantProduct p : shortestProducts) {
            //System.out.println("Product");
            for(Implicant i : p.getImplicants()) {
                //System.out.println(i.getMask() + i.getRowIndices());
            }
        }

        ImplicantProduct productWithLeastLiterals =
                getFirstProductWithLeastLiterals(shortestProducts);

        return productWithLeastLiterals.getImplicants();
    }

    public TruthTableData apply(TruthTableData inputTableData, boolean outputValue) throws InterruptedException {
        ImplicantTable implicantTable = new ImplicantTable(inputTableData.inputCount());

        createFirstLevelOfImplicants(
                implicantTable,
                inputTableData.stream()
                        .filter(row -> row.getOutput() == TriState.fromBoolean(outputValue))
                        .collect(Collectors.toList()),
                outputValue);

        fullyCombineImplicants(implicantTable);

        PrimeImplicantTable primeChart =
                implicantTable.toPrimeImplicantTable();
        primeChart.print();
        PrimeImplicantTable.EssentialAndRegularImplicants essentialAndRegularImplicants =
                primeChart.splitToEssentialAndRegularImplicants();

//        System.out.println("Essential implicants: ");
//        for(Implicant i : essentialAndRegularImplicants.essentials) {
//            System.out.println(i.getMask());
//        }

        if (essentialAndRegularImplicants.regulars.isEmpty()) {
//            System.out.println("no petrick");
            return makeTruthTable(essentialAndRegularImplicants.essentials, outputValue);
        }

        Set<Implicant> bestMatchingImplicants =
                runPetricksMethod(essentialAndRegularImplicants.regulars);


//        System.out.println("Best matching implicants:");
//        for(Implicant i : bestMatchingImplicants) {
//            System.out.println(i.getMask());
//        }

        Collection<Implicant> resultImplicants =
                Containers.join(essentialAndRegularImplicants.essentials, bestMatchingImplicants);

        return makeTruthTable(resultImplicants, outputValue);
    }
}

class Containers {
    public static <T> Collection<T> join(Collection<T> left, Collection<T> right) {
        ArrayList<T> result = new ArrayList<>(left);
        result.addAll(right);
        return result;
    }
}

class ImplicantProduct {
    public ImplicantProduct(QuineMcCluskey.Implicant implicant) {
        implicants = new HashSet<>();
        implicants.add(implicant);
    }
    public ImplicantProduct(Set<QuineMcCluskey.Implicant> implicants) {
        this.implicants = implicants;
    }
    public ImplicantProduct mul(ImplicantProduct other) {
        HashSet<QuineMcCluskey.Implicant> netImplicants = new HashSet<>(implicants);
        netImplicants.addAll(other.implicants);
        return new ImplicantProduct(netImplicants);
    }
    public Set<QuineMcCluskey.Implicant> getImplicants() {
        return implicants;
    }
    public int size() {
        return implicants.size();
    }
    public int getLiteralsCount() {
        return implicants.stream().map(QuineMcCluskey.Implicant::getLiteralsCount).reduce(Integer::sum).get();
    }
    private final Set<QuineMcCluskey.Implicant> implicants;
}

class ImplicantSum {

    public ImplicantSum(Set<ImplicantProduct> prods) {
        this.prods = prods;
    }
    public ImplicantSum mul(ImplicantSum other) {
        HashSet<ImplicantProduct> netProds = new HashSet<ImplicantProduct>();
        ArrayList<ImplicantProduct> listProds = new ArrayList<ImplicantProduct>();
        for(ImplicantProduct thisProd : prods) {
            for(ImplicantProduct otherProd : other.prods) {
                ImplicantProduct netProd = thisProd.mul(otherProd);
                netProds.add(netProd);
                listProds.add(netProd);
            }
        }
        for(int i = 0; i < listProds.size()-1; i++) {
            for(int j = i+1; j < listProds.size(); j++) {
                ImplicantProduct p1 = listProds.get(i);
                ImplicantProduct p2 = listProds.get(j);
                ImplicantProduct min = p1.getImplicants().size() > p2.getImplicants().size() ? p2 : p1;
                ImplicantProduct max = p1 == min ? p2 : p1;
                if (max.getImplicants().containsAll(min.getImplicants())) {
                    netProds.remove(max);
                    break;
                }
            }
        }
        return new ImplicantSum(netProds);
    }

    public Set<ImplicantProduct> getProducts() {
        return prods;
    }

    private final Set<ImplicantProduct> prods;
}

