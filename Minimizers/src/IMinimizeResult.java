public interface IMinimizeResult {
    TruthTable getTruthTable();

    SymbolicExpression getExpression();
}
