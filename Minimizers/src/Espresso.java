import org.apache.commons.lang3.SystemUtils;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Espresso {
    private AtomicReference<Process> process = new AtomicReference<>();

    public Espresso() {
        ensureProcessDestroyedOnProgramShutdown();
    }

    public TruthTableData apply(
            TruthTableData inputTable, boolean minimizationTarget)
            throws IOException, InterruptedException, UnsupportedPlatformException, URISyntaxException {

        String espressoCommandString =
                prepareCommand(inputTable, minimizationTarget);

        process.set(startProcess(espressoCommandString));

        TruthTableData outputTableRows = new TruthTableData();
        try (BufferedReader processResponseStream = prepareInputReader()) {
            while (true) {
                boolean alive = process.get().isAlive();
                if (!alive) {
                    break;
                }

                boolean startedReadingInput = false;
                while (true) {
                    String line = processResponseStream.readLine();

                    if (line == null || line.startsWith(".e")) {
                        break;
                    } else if (line.startsWith(".p")) {
                        startedReadingInput = true;
                    } else if (startedReadingInput) {
                        outputTableRows.add(parseTruthTableRow(line, minimizationTarget));
                    }
                }

            }
        }
        return outputTableRows;
    }

    public void cleanupFile() {
        if (inputFile == null) {
            return;
        }
        File f = inputFile.toFile();
        if (!f.exists()) {
            return;
        }
        inputFile.toFile().delete();
    }

    public void destroyProcess() throws InterruptedException {
        if (process.get() != null) {
            process.get().destroy();
            process.set(null);
        }
    }

    private void ensureProcessDestroyedOnProgramShutdown() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (process.get() != null && process.get().isAlive()) {
                process.get().destroy();
            }
        }));
    }
    private Path inputFile;
    private void prepareEspressoInputFile(
            TruthTableData inputTable) throws IOException {

        inputFile = Files.createTempFile(null, ".input");

        try (FileWriter writer = new FileWriter(inputFile.toString(), false)) {
            String espressoInputHeader =
                    prepareEspressoInputHeader(inputTable);

            writer.write(espressoInputHeader);

            String espressoTruthTable =
                    prepareEspressoTruthTable(inputTable);

            writer.write(espressoTruthTable);

            String espressoInputFooter =
                    prepareEspressoInputFooter();

            writer.write(espressoInputFooter);
        }
    }

    private String prepareEspressoInputHeader(TruthTableData inputTable) {
        return new StringBuilder()
                .append(".i ").append(inputTable.inputCount()).append('\n')
                .append(".o 1\n")
                .toString();
    }

    private String prepareEspressoTruthTable(TruthTableData inputTable) {
        StringBuilder sb = new StringBuilder();
        inputTable.forEach(row -> {
            row.getInputs().stream()
                    .map(TriState::toChar)
                    .forEach(sb::append);
            sb.append(' ');
            sb.append(row.getOutput().toChar());
            sb.append('\n');
        });
        return sb.toString();
    }

    private String prepareEspressoInputFooter() {
        return ".e\n";
    }

    private String prepareEspressoOptions(boolean outputValue) {
        return outputValue ? "" : "-epos"; // minimize for OFF state and display it
    }

    private File getEspressoFile() throws URISyntaxException {
        return new File(
                Espresso.class.getProtectionDomain().getCodeSource().getLocation().toURI().resolve(".").getPath(),
                getPlatformDependentFileName());
    }

    private String prepareCommand(
            TruthTableData inputTable, boolean minimizationTarget) throws IOException, UnsupportedPlatformException, URISyntaxException {

        File executable = getEspressoFile();

        String options =
                prepareEspressoOptions(minimizationTarget);

        prepareEspressoInputFile(inputTable);

        return executable.getPath()
                + (options.isEmpty() ? "" : " " + options)
                + (" " + inputFile);
    }

    private Process startProcess(String command) throws IOException, InterruptedException {
        return Runtime.getRuntime().exec(command);
    }

    private BufferedReader prepareInputReader() throws InterruptedException {
        InputStream inputStream = process.get().getInputStream();
        return new BufferedReader(new InputStreamReader(inputStream));
    }

    private TruthTableRow parseTruthTableRow(String s, boolean target) {

        List<TriState> inputs = s.substring(0, s.length() - 2)
                .chars()
                .mapToObj(TriState::fromChar)
                .collect(Collectors.toList());

        TriState output = TriState.fromBoolean(target);

        return new TruthTableRow(inputs, output);
    }

    private static String getPlatformDependentFileName() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return "windows/espresso.exe";
        } else if (SystemUtils.IS_OS_LINUX) {
            return "linux/espresso";
        } else if (SystemUtils.IS_OS_MAC) {
            return "mac/espresso";
        } else {
            throw new UnsupportedPlatformException();
        }
    }
}
