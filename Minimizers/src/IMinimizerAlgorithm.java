import java.io.IOException;
import java.net.URISyntaxException;

public interface IMinimizerAlgorithm {
    IMinimizeResult run(TruthTable truthTable, boolean targetOutput)
            throws IOException, InterruptedException, URISyntaxException;
}


