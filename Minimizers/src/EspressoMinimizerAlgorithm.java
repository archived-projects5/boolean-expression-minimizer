import java.io.IOException;
import java.net.URISyntaxException;

public class EspressoMinimizerAlgorithm implements IMinimizerAlgorithm {

    private Espresso esp = null;

    @Override
    public IMinimizeResult run(TruthTable inputTable, boolean targetOutputValue )
            throws IOException, InterruptedException, UnsupportedPlatformException, URISyntaxException {

        esp = new Espresso();

        TruthTableData outputTableData = esp.apply(inputTable.getData(), targetOutputValue);
        TruthTable outputTable = new TruthTable(inputTable.getHeaderRow(), outputTableData);

        return new IMinimizeResult() {

            @Override
            public TruthTable getTruthTable() {
                return outputTable;
            }

            @Override
            public SymbolicExpression getExpression() {
                return outputTable.toSymbolicExpression(targetOutputValue
                        ? SymbolicExpression.Type.Disjunctive
                        : SymbolicExpression.Type.Conjunctive);
            }
        };
    }

    public void destroyProcess() {
        if (esp != null) {
            try {
                esp.destroyProcess();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void cleanup() {
        esp.cleanupFile();
    }
}
