
public class QuineMcCluskeyMinimizerAlgorithm implements IMinimizerAlgorithm {

    @Override
    public IMinimizeResult run(TruthTable inputTable, boolean targetOutputValue) throws InterruptedException {

        QuineMcCluskey qmc = new QuineMcCluskey();
        TruthTableData outputData = qmc.apply(inputTable.getData(), targetOutputValue);
        TruthTable outputTable = new TruthTable(inputTable.getHeaderRow(), outputData);

        return new IMinimizeResult() {

            @Override
            public TruthTable getTruthTable() {
                return outputTable;
            }

            @Override
            public SymbolicExpression getExpression() {
                return outputTable.toSymbolicExpression(targetOutputValue
                        ? SymbolicExpression.Type.Disjunctive
                        : SymbolicExpression.Type.Conjunctive);
            }

        };
    }
}
