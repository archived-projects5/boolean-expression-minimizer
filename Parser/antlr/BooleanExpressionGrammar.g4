grammar BooleanExpressionGrammar;

terminatedExpression
    : expression EOF    #realExpression
    | EOF               #nullExpression
    ;

expression
    : unitExpression                                #unitExpression2
    | left=expression op=XOR right=expression       #xorExpression
    | left=expression right=expression              #implicitAndExpression
    | left=expression op=AND right=expression       #explicitAndExpression
    | left=expression op=OR right=expression        #orExpression
    ;

unitExpression
    : LPAR expression RPAR      #parenthesizedExpression
    | VAR                       #variableExpression
    | ONE                       #oneExpression
    | ZERO                      #zeroExpression
    | NOT unitExpression        #negatedUnitExpression
    ;

VAR
    : [a-zA-Z]
    | [a-zA-Z][0-9]
    | [a-zA-Z][0-9][0-9]
    | [a-zA-Z][0-9][0-9][0-9]
    | [a-zA-Z][0-9][0-9][0-9][0-9]
    ;


ONE: '1';
ZERO: '0';

NOT: [!~/];
AND: [*&];
OR: [+|];
XOR: '^';
LPAR: '(';
RPAR: ')';

WS: [ \t\n\f\r] -> skip;
