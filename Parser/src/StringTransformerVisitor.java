import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;

public class StringTransformerVisitor extends BooleanExpressionGrammarBaseVisitor<String> {

    public StringTransformerVisitor(ExpressionSymbols symbols) {
        this.symbols = symbols;
    }

    @Override
    public String visitOneExpression(BooleanExpressionGrammarParser.OneExpressionContext ctx) {
        return symbols.one;
    }

    @Override
    public String visitZeroExpression(BooleanExpressionGrammarParser.ZeroExpressionContext ctx) {
        return symbols.zero;
    }

    @Override
    public String visitRealExpression(BooleanExpressionGrammarParser.RealExpressionContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public String visitNullExpression(BooleanExpressionGrammarParser.NullExpressionContext ctx) {
        return "";
    }

    @Override
    public String visitImplicitAndExpression(BooleanExpressionGrammarParser.ImplicitAndExpressionContext ctx) {
        return visit(ctx.left) + symbols.and + visit(ctx.right);
    }

    @Override
    public String visitExplicitAndExpression(BooleanExpressionGrammarParser.ExplicitAndExpressionContext ctx) {
        return visit(ctx.left) + symbols.and + visit(ctx.right);
    }

    @Override
    public String visitXorExpression(BooleanExpressionGrammarParser.XorExpressionContext ctx) {
        return visit(ctx.left) + symbols.xor + visit(ctx.right);
    }

    @Override
    public String visitOrExpression(BooleanExpressionGrammarParser.OrExpressionContext ctx) {
        return visit(ctx.left) + symbols.or + visit(ctx.right);
    }

    @Override
    public String visitParenthesizedExpression(BooleanExpressionGrammarParser.ParenthesizedExpressionContext ctx) {
        return symbols.parenBegin + visit(ctx.expression()) + symbols.parenEnd;
    }

    @Override
    public String visitVariableExpression(BooleanExpressionGrammarParser.VariableExpressionContext ctx) {
        StringBuilder sb = new StringBuilder();

        String str = ctx.VAR().getSymbol().getText();

        sb.append(symbols.idBegin);
        sb.append(str.charAt(0));
        sb.append(symbols.idEnd);

        String index = str.substring(1);

        if (index != null && !index.equals("")) {
            sb.append(symbols.indexBegin);
            sb.append(index);
            sb.append(symbols.indexEnd);
        }

        return sb.toString();
    }

    @Override
    public String visitNegatedUnitExpression(BooleanExpressionGrammarParser.NegatedUnitExpressionContext ctx) {
        if (ctx.unitExpression() instanceof BooleanExpressionGrammarParser.ParenthesizedExpressionContext) {
            BooleanExpressionGrammarParser.ParenthesizedExpressionContext parenExpr = (BooleanExpressionGrammarParser.ParenthesizedExpressionContext)ctx.unitExpression();
            return symbols.multiNegationBegin + visit(parenExpr.expression()) + symbols.multiNegationEnd;
        }
        else {
            return symbols.singleNegationBegin + visit(ctx.unitExpression()) + symbols.singleNegationEnd;
        }
    }

    @Override
    public String visitChildren(RuleNode node) {
        String result = this.defaultResult();
        int n = node.getChildCount();
        for(int i = 0; i < n && this.shouldVisitNextChild(node, result); ++i) {
            ParseTree c = node.getChild(i);
            String childResult = c.accept(this);
            result = this.aggregateResult(result, childResult);
        }
        return result != null ? result : "?";
    }


    private final ExpressionSymbols symbols;
}
