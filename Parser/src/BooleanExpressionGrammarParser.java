// Generated from C:/Users/Filip/Desktop/goo/boolean-expression-minimizer/Parser/antlr\BooleanExpressionGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BooleanExpressionGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		VAR=1, ONE=2, ZERO=3, NOT=4, AND=5, OR=6, XOR=7, LPAR=8, RPAR=9, WS=10;
	public static final int
		RULE_terminatedExpression = 0, RULE_expression = 1, RULE_unitExpression = 2;
	private static String[] makeRuleNames() {
		return new String[] {
			"terminatedExpression", "expression", "unitExpression"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, "'1'", "'0'", null, null, null, "'^'", "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "VAR", "ONE", "ZERO", "NOT", "AND", "OR", "XOR", "LPAR", "RPAR", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BooleanExpressionGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BooleanExpressionGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class TerminatedExpressionContext extends ParserRuleContext {
		public TerminatedExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_terminatedExpression; }
	 
		public TerminatedExpressionContext() { }
		public void copyFrom(TerminatedExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NullExpressionContext extends TerminatedExpressionContext {
		public TerminalNode EOF() { return getToken(BooleanExpressionGrammarParser.EOF, 0); }
		public NullExpressionContext(TerminatedExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitNullExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealExpressionContext extends TerminatedExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(BooleanExpressionGrammarParser.EOF, 0); }
		public RealExpressionContext(TerminatedExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitRealExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TerminatedExpressionContext terminatedExpression() throws RecognitionException {
		TerminatedExpressionContext _localctx = new TerminatedExpressionContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_terminatedExpression);
		try {
			setState(10);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VAR:
			case ONE:
			case ZERO:
			case NOT:
			case LPAR:
				_localctx = new RealExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(6);
				expression(0);
				setState(7);
				match(EOF);
				}
				break;
			case EOF:
				_localctx = new NullExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(9);
				match(EOF);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ImplicitAndExpressionContext extends ExpressionContext {
		public ExpressionContext left;
		public ExpressionContext right;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ImplicitAndExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitImplicitAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExplicitAndExpressionContext extends ExpressionContext {
		public ExpressionContext left;
		public Token op;
		public ExpressionContext right;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AND() { return getToken(BooleanExpressionGrammarParser.AND, 0); }
		public ExplicitAndExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitExplicitAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrExpressionContext extends ExpressionContext {
		public ExpressionContext left;
		public Token op;
		public ExpressionContext right;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode OR() { return getToken(BooleanExpressionGrammarParser.OR, 0); }
		public OrExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class XorExpressionContext extends ExpressionContext {
		public ExpressionContext left;
		public Token op;
		public ExpressionContext right;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode XOR() { return getToken(BooleanExpressionGrammarParser.XOR, 0); }
		public XorExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitXorExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnitExpression2Context extends ExpressionContext {
		public UnitExpressionContext unitExpression() {
			return getRuleContext(UnitExpressionContext.class,0);
		}
		public UnitExpression2Context(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitUnitExpression2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new UnitExpression2Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(13);
			unitExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(28);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(26);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new XorExpressionContext(new ExpressionContext(_parentctx, _parentState));
						((XorExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(15);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(16);
						((XorExpressionContext)_localctx).op = match(XOR);
						setState(17);
						((XorExpressionContext)_localctx).right = expression(5);
						}
						break;
					case 2:
						{
						_localctx = new ImplicitAndExpressionContext(new ExpressionContext(_parentctx, _parentState));
						((ImplicitAndExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(18);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(19);
						((ImplicitAndExpressionContext)_localctx).right = expression(4);
						}
						break;
					case 3:
						{
						_localctx = new ExplicitAndExpressionContext(new ExpressionContext(_parentctx, _parentState));
						((ExplicitAndExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(20);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(21);
						((ExplicitAndExpressionContext)_localctx).op = match(AND);
						setState(22);
						((ExplicitAndExpressionContext)_localctx).right = expression(3);
						}
						break;
					case 4:
						{
						_localctx = new OrExpressionContext(new ExpressionContext(_parentctx, _parentState));
						((OrExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(23);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(24);
						((OrExpressionContext)_localctx).op = match(OR);
						setState(25);
						((OrExpressionContext)_localctx).right = expression(2);
						}
						break;
					}
					} 
				}
				setState(30);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnitExpressionContext extends ParserRuleContext {
		public UnitExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unitExpression; }
	 
		public UnitExpressionContext() { }
		public void copyFrom(UnitExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OneExpressionContext extends UnitExpressionContext {
		public TerminalNode ONE() { return getToken(BooleanExpressionGrammarParser.ONE, 0); }
		public OneExpressionContext(UnitExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitOneExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenthesizedExpressionContext extends UnitExpressionContext {
		public TerminalNode LPAR() { return getToken(BooleanExpressionGrammarParser.LPAR, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(BooleanExpressionGrammarParser.RPAR, 0); }
		public ParenthesizedExpressionContext(UnitExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitParenthesizedExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NegatedUnitExpressionContext extends UnitExpressionContext {
		public TerminalNode NOT() { return getToken(BooleanExpressionGrammarParser.NOT, 0); }
		public UnitExpressionContext unitExpression() {
			return getRuleContext(UnitExpressionContext.class,0);
		}
		public NegatedUnitExpressionContext(UnitExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitNegatedUnitExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ZeroExpressionContext extends UnitExpressionContext {
		public TerminalNode ZERO() { return getToken(BooleanExpressionGrammarParser.ZERO, 0); }
		public ZeroExpressionContext(UnitExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitZeroExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableExpressionContext extends UnitExpressionContext {
		public TerminalNode VAR() { return getToken(BooleanExpressionGrammarParser.VAR, 0); }
		public VariableExpressionContext(UnitExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionGrammarVisitor ) return ((BooleanExpressionGrammarVisitor<? extends T>)visitor).visitVariableExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnitExpressionContext unitExpression() throws RecognitionException {
		UnitExpressionContext _localctx = new UnitExpressionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_unitExpression);
		try {
			setState(40);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LPAR:
				_localctx = new ParenthesizedExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(31);
				match(LPAR);
				setState(32);
				expression(0);
				setState(33);
				match(RPAR);
				}
				break;
			case VAR:
				_localctx = new VariableExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(35);
				match(VAR);
				}
				break;
			case ONE:
				_localctx = new OneExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(36);
				match(ONE);
				}
				break;
			case ZERO:
				_localctx = new ZeroExpressionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(37);
				match(ZERO);
				}
				break;
			case NOT:
				_localctx = new NegatedUnitExpressionContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(38);
				match(NOT);
				setState(39);
				unitExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		case 2:
			return precpred(_ctx, 2);
		case 3:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\f-\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\3\2\3\2\3\2\3\2\5\2\r\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\7\3\35\n\3\f\3\16\3 \13\3\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\5\4+\n\4\3\4\2\3\4\5\2\4\6\2\2\2\62\2\f\3\2\2\2\4\16\3"+
		"\2\2\2\6*\3\2\2\2\b\t\5\4\3\2\t\n\7\2\2\3\n\r\3\2\2\2\13\r\7\2\2\3\f\b"+
		"\3\2\2\2\f\13\3\2\2\2\r\3\3\2\2\2\16\17\b\3\1\2\17\20\5\6\4\2\20\36\3"+
		"\2\2\2\21\22\f\6\2\2\22\23\7\t\2\2\23\35\5\4\3\7\24\25\f\5\2\2\25\35\5"+
		"\4\3\6\26\27\f\4\2\2\27\30\7\7\2\2\30\35\5\4\3\5\31\32\f\3\2\2\32\33\7"+
		"\b\2\2\33\35\5\4\3\4\34\21\3\2\2\2\34\24\3\2\2\2\34\26\3\2\2\2\34\31\3"+
		"\2\2\2\35 \3\2\2\2\36\34\3\2\2\2\36\37\3\2\2\2\37\5\3\2\2\2 \36\3\2\2"+
		"\2!\"\7\n\2\2\"#\5\4\3\2#$\7\13\2\2$+\3\2\2\2%+\7\3\2\2&+\7\4\2\2\'+\7"+
		"\5\2\2()\7\6\2\2)+\5\6\4\2*!\3\2\2\2*%\3\2\2\2*&\3\2\2\2*\'\3\2\2\2*("+
		"\3\2\2\2+\7\3\2\2\2\6\f\34\36*";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}