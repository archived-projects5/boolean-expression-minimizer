import java.util.List;

public interface IParsedExpression {
    String getText(ExpressionSymbols symbols);

    List<CharCoords> getErrors();

    boolean canBeEvaluated();

    TruthTable evaluate() throws InterruptedException;
}


