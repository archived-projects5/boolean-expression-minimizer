// Generated from C:/Users/Filip/Desktop/goo/boolean-expression-minimizer/Parser/antlr\BooleanExpressionGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BooleanExpressionGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BooleanExpressionGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code realExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#terminatedExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealExpression(BooleanExpressionGrammarParser.RealExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#terminatedExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullExpression(BooleanExpressionGrammarParser.NullExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code implicitAndExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplicitAndExpression(BooleanExpressionGrammarParser.ImplicitAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code explicitAndExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExplicitAndExpression(BooleanExpressionGrammarParser.ExplicitAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrExpression(BooleanExpressionGrammarParser.OrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code xorExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitXorExpression(BooleanExpressionGrammarParser.XorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unitExpression2}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnitExpression2(BooleanExpressionGrammarParser.UnitExpression2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code parenthesizedExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenthesizedExpression(BooleanExpressionGrammarParser.ParenthesizedExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableExpression(BooleanExpressionGrammarParser.VariableExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oneExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOneExpression(BooleanExpressionGrammarParser.OneExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code zeroExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZeroExpression(BooleanExpressionGrammarParser.ZeroExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negatedUnitExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegatedUnitExpression(BooleanExpressionGrammarParser.NegatedUnitExpressionContext ctx);
}