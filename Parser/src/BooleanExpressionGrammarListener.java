// Generated from C:/Users/Filip/IdeaProjects/BooleanExpressionMinimizer/Parser/antlr\BooleanExpressionGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BooleanExpressionGrammarParser}.
 */
public interface BooleanExpressionGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code realExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#terminatedExpression}.
	 * @param ctx the parse tree
	 */
	void enterRealExpression(BooleanExpressionGrammarParser.RealExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code realExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#terminatedExpression}.
	 * @param ctx the parse tree
	 */
	void exitRealExpression(BooleanExpressionGrammarParser.RealExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#terminatedExpression}.
	 * @param ctx the parse tree
	 */
	void enterNullExpression(BooleanExpressionGrammarParser.NullExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#terminatedExpression}.
	 * @param ctx the parse tree
	 */
	void exitNullExpression(BooleanExpressionGrammarParser.NullExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code implicitAndExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterImplicitAndExpression(BooleanExpressionGrammarParser.ImplicitAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code implicitAndExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitImplicitAndExpression(BooleanExpressionGrammarParser.ImplicitAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code explicitAndExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExplicitAndExpression(BooleanExpressionGrammarParser.ExplicitAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code explicitAndExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExplicitAndExpression(BooleanExpressionGrammarParser.ExplicitAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(BooleanExpressionGrammarParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(BooleanExpressionGrammarParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code xorExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterXorExpression(BooleanExpressionGrammarParser.XorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code xorExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitXorExpression(BooleanExpressionGrammarParser.XorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unitExpression2}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnitExpression2(BooleanExpressionGrammarParser.UnitExpression2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code unitExpression2}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnitExpression2(BooleanExpressionGrammarParser.UnitExpression2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code parenthesizedExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 */
	void enterParenthesizedExpression(BooleanExpressionGrammarParser.ParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenthesizedExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 */
	void exitParenthesizedExpression(BooleanExpressionGrammarParser.ParenthesizedExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 */
	void enterVariableExpression(BooleanExpressionGrammarParser.VariableExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 */
	void exitVariableExpression(BooleanExpressionGrammarParser.VariableExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code negatedUnitExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 */
	void enterNegatedUnitExpression(BooleanExpressionGrammarParser.NegatedUnitExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code negatedUnitExpression}
	 * labeled alternative in {@link BooleanExpressionGrammarParser#unitExpression}.
	 * @param ctx the parse tree
	 */
	void exitNegatedUnitExpression(BooleanExpressionGrammarParser.NegatedUnitExpressionContext ctx);
}