import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class VariableSeekerVisitor extends BooleanExpressionGrammarBaseVisitor<Void> {

    @Override
    public Void visitVariableExpression(BooleanExpressionGrammarParser.VariableExpressionContext ctx) {
        variables.add(ctx.VAR().getText());
        return null;
    }

    public List<String> getVariables() {
        return new ArrayList<>(variables);
    }

    private final Set<String> variables = new HashSet<>();
}
