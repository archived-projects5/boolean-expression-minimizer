import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.HashMap;
import java.util.Map;

public class ExpressionEvaluatorVisitor extends BooleanExpressionGrammarBaseVisitor<Boolean> {

    private final Map<String, Boolean> inputMap;

    public ExpressionEvaluatorVisitor(
            HashMap<String, Boolean> inputMap) {
        this.inputMap = inputMap;
    }

    @Override
    public Boolean visit(ParseTree tree) {
        if (Thread.interrupted()) {
            throw new ParseCancellationException();
        }
        return super.visit(tree);
    }

    @Override
    public Boolean visitOneExpression(BooleanExpressionGrammarParser.OneExpressionContext ctx) {
        return true;
    }

    @Override
    public Boolean visitZeroExpression(BooleanExpressionGrammarParser.ZeroExpressionContext ctx) {
        return false;
    }

    @Override
    public Boolean visitRealExpression(BooleanExpressionGrammarParser.RealExpressionContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public Boolean visitImplicitAndExpression(BooleanExpressionGrammarParser.ImplicitAndExpressionContext ctx) {
        return visit(ctx.left) && visit(ctx.right);
    }

    @Override
    public Boolean visitExplicitAndExpression(BooleanExpressionGrammarParser.ExplicitAndExpressionContext ctx) {
        return visit(ctx.left) && visit(ctx.right);
    }

    @Override
    public Boolean visitOrExpression(BooleanExpressionGrammarParser.OrExpressionContext ctx) {
        return visit(ctx.left) || visit(ctx.right);
    }

    @Override
    public Boolean visitXorExpression(BooleanExpressionGrammarParser.XorExpressionContext ctx) {
        return visit(ctx.left) ^ visit(ctx.right);
    }

    @Override
    public Boolean visitParenthesizedExpression(BooleanExpressionGrammarParser.ParenthesizedExpressionContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public Boolean visitNegatedUnitExpression(BooleanExpressionGrammarParser.NegatedUnitExpressionContext ctx) {
        return !visit(ctx.unitExpression());
    }

    @Override
    public Boolean visitVariableExpression(BooleanExpressionGrammarParser.VariableExpressionContext ctx) {
        return inputMap.get(ctx.VAR().getText());
    }
}
