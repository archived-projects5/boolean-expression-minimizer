import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;

import java.util.ArrayList;
import java.util.List;

public class ParserErrorVisitor extends BooleanExpressionGrammarBaseVisitor<Void> {

    @Override
    public Void visitErrorNode(ErrorNode node) {
        Token errorToken = node.getSymbol();
        errors.add(new CharCoords(errorToken.getLine(), errorToken.getCharPositionInLine()));
        return super.visitErrorNode(node);
    }

    public List<CharCoords> getErrors() {
        return errors;
    }

    private final List<CharCoords> errors = new ArrayList<>();
}
