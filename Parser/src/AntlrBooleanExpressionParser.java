import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.*;
import java.util.stream.Collectors;

public class AntlrBooleanExpressionParser {
    private ParseTree parseTree = null;

    IParsedExpression parse(String expr) {
        CodePointCharStream charStream = CharStreams.fromString(expr);

        LexerErrorListener lexerErrorListener = new LexerErrorListener();

        BooleanExpressionGrammarLexer lexer = new BooleanExpressionGrammarLexer(charStream);
        lexer.removeErrorListeners();
        lexer.addErrorListener(lexerErrorListener);

        CommonTokenStream antlrTokens = new CommonTokenStream(lexer);

        BooleanExpressionGrammarParser parser = new BooleanExpressionGrammarParser(antlrTokens);
        parser.removeErrorListeners();
        parser.addErrorListener(lexerErrorListener);

        parseTree = parser.terminatedExpression();

        ParserErrorVisitor parserErrorVisitor = new ParserErrorVisitor();
        parserErrorVisitor.visit(parseTree);

        return new IParsedExpression() {

            @Override
            public String getText(ExpressionSymbols symbols) {
                StringTransformerVisitor stringTransformerVisitor = new StringTransformerVisitor(symbols);
                return stringTransformerVisitor.visit(parseTree);
            }

            @Override
            public List<CharCoords> getErrors() {
                ArrayList<CharCoords> errors = new ArrayList<CharCoords>();
                errors.addAll(lexerErrorListener.getErrors());
                errors.addAll(parserErrorVisitor.getErrors());
                return errors;
            }

            @Override
            public boolean canBeEvaluated() {
                return getErrors().isEmpty() && !parseTree.getText().equals("<EOF>");
            }

            @Override
            public TruthTable evaluate() throws InterruptedException {
                return createTruthTable(parseTree);
            }
        };
    }

    private TruthTable createTruthTable(ParseTree parseTree) throws InterruptedException {
        VariableSeekerVisitor variableSeekerVisitor = new VariableSeekerVisitor();
        variableSeekerVisitor.visit(parseTree);

        List<String> truthTableHeaders = variableSeekerVisitor.getVariables();
        TruthTableData truthTableData = new TruthTableData();

        for(int i = 0; i < (int)Math.pow(2, truthTableHeaders.size()); i++) {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            List<Boolean> permutation = BooleanUtils.getPermutation(i, truthTableHeaders.size());

            HashMap<String, Boolean> inputMap = new HashMap<String, Boolean>();
            for(int j = 0; j < truthTableHeaders.size(); j++) {
                String name = truthTableHeaders.get(j);
                Boolean value = permutation.get(j);
                inputMap.put(name, value);
            }

            ExpressionEvaluatorVisitor evaluatorVisitor = new ExpressionEvaluatorVisitor(inputMap);
            Boolean output = evaluatorVisitor.visit(parseTree);

            truthTableData.add(new TruthTableRow(
                    permutation.stream().map(TriState::fromBoolean).collect(Collectors.toList()),
                    TriState.fromBoolean(output)
            ));
        }

        return new TruthTable(truthTableHeaders, truthTableData);
    }
}

