public class CharCoords {
    public CharCoords(int line, int column) {
        this.line = line;
        this.column = column;
    }

    public int line;
    public int column;
}
