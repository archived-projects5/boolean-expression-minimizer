public class ExpressionSymbols {

    public ExpressionSymbols(
            String indexBegin,
            String indexEnd,
            String singleNegationBegin,
            String singleNegationEnd,
            String multiNegationBegin,
            String multiNegationEnd,
            String xor,
            String and,
            String or,
            String parenBegin,
            String parenEnd,
            String idBegin,
            String idEnd,
            String one,
            String zero
            ) {
        this.parenBegin = parenBegin;
        this.parenEnd = parenEnd;
        this.idBegin = idBegin;
        this.idEnd = idEnd;
        this.indexBegin = indexBegin;
        this.indexEnd = indexEnd;
        this.singleNegationBegin = singleNegationBegin;
        this.singleNegationEnd = singleNegationEnd;
        this.multiNegationBegin = multiNegationBegin;
        this.multiNegationEnd = multiNegationEnd;
        this.xor = xor;
        this.and = and;
        this.or = or;
        this.one = one;
        this.zero = zero;
    }

    public final String parenBegin;
    public final String parenEnd;
    public final String idBegin;
    public final String idEnd;
    public final String indexBegin;
    public final String indexEnd;
    public final String singleNegationBegin;
    public final String singleNegationEnd;
    public final String multiNegationBegin;
    public final String multiNegationEnd;
    public final String xor;
    public final String and;
    public final String or;
    public final String one;
    public final String zero;
}
