import java.util.ArrayList;

public class TruthTableData extends ArrayList<TruthTableRow> {

    public int inputCount() {
        return get(0).getInputs().size();
    }

    public int rowCount() {
        return size();
    }
}
