import java.util.List;

public class SymbolicExpression {

    public static SymbolicExpression one() {
        return new SymbolicExpression(SpecialType.One, null, null);
    }

    public static SymbolicExpression zero() {
        return new SymbolicExpression(SpecialType.Zero, null, null);
    }

    public enum SpecialType { One, Zero }
    public enum Type { Conjunctive, Disjunctive }

    public static class Term {

        public static class Variable {

            public Variable(String name, boolean negated) {
                this.name = name;
                this.negated = negated;
            }
            public final boolean negated;
            public final String name;
        }

        public Term(List<Variable> variables) {
            this.variables = variables;
        }

        public final List<Variable> variables;

    }

    public static SymbolicExpression create(Type type, List<Term> terms) {
        return new SymbolicExpression(null, type, terms);
    }


    private SymbolicExpression(SpecialType specialValue, Type type, List<Term> terms) {
        this.type = type;
        this.terms = terms;
        this.specialValue = specialValue;
    }

    public final Type type;
    public final List<Term> terms;
    public final SpecialType specialValue;
}

