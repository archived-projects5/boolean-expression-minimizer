public enum TriState {
    _0, _1, _X;

    public static TriState fromBoolean(boolean b) {
        return b ? _1 : _0;
    }

    public static TriState fromChar(int c) {
        switch(c) {
            case '1': return _1;
            case '0': return _0;
            case '-': return _X;
            default: throw new RuntimeException(
                    "Cannot create TriState from '" + c + "'");
        }
    }

    public boolean toBoolean() {
        switch (this) {
            case _1: return true;
            case _0: return false;
            default: throw new RuntimeException(
                    "Cannot convert '" + name() + "' to boolean");
        }
    }

    public char toChar() {
        switch(this) {
            case _1: return '1';
            case _0: return '0';
            case _X: return '-';
            default: throw new RuntimeException(
                    "Cannot convert '" + name() + "' to char");
        }
    }
}

