import java.util.List;

public class TruthTableRow {
    public TruthTableRow(List<TriState> inputs, TriState output) {
        this.inputs = inputs;
        this.output = output;
    }

    public TriState getOutput() {
        return output;
    }

    public List<TriState> getInputs() {
        return inputs;
    }

    private final List<TriState> inputs;
    private final TriState output;
}
