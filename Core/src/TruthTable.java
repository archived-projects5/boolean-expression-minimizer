import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TruthTable {

    public TruthTable(List<String> headerRow, TruthTableData dataRows) {
        assert !headerRow.isEmpty();
        assert !dataRows.isEmpty();

        this.headerRow = new ArrayList<>(headerRow);
        this.dataRows = dataRows;
    }

    public List<String> getHeaderRow() {
        return headerRow;
    }

    public TruthTableData getData() {
        return dataRows;
    }

    public SymbolicExpression toSymbolicExpression(SymbolicExpression.Type type) {

        boolean negateOnes = type == SymbolicExpression.Type.Conjunctive;

        List<SymbolicExpression.Term> terms = dataRows
                .stream()
                .map(row -> {

                    ArrayList<SymbolicExpression.Term.Variable> variables = new ArrayList<SymbolicExpression.Term.Variable>();

                    IntStream.range(0, row.getInputs().size()).forEach(i -> {
                        switch (row.getInputs().get(i)) {
                            case _1:
                                variables.add(new SymbolicExpression.Term.Variable(
                                        headerRow.get(i), negateOnes));
                                break;
                            case _0:
                                variables.add(new SymbolicExpression.Term.Variable(
                                        headerRow.get(i), !negateOnes));
                                break;
                            case _X:
                                break;
                        }
                    });

                    return new SymbolicExpression.Term(variables);

                }).collect(Collectors.toList());


        if (terms.isEmpty()) {
            if (type == SymbolicExpression.Type.Conjunctive) {
                return SymbolicExpression.one();
            } else {
                return SymbolicExpression.zero();
            }
        } else if (terms.size() == 1 && terms.get(0).variables.isEmpty()) {
            if (type == SymbolicExpression.Type.Conjunctive) {
                return SymbolicExpression.zero();
            } else {
                return SymbolicExpression.one();
            }
        }

        return SymbolicExpression.create(type, terms);
    }

    private final List<String> headerRow;

    private final TruthTableData dataRows;
}
