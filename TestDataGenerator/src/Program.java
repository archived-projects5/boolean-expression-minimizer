import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class Program {

    private static final Random rng = new Random(42);

    public static void main(String[] args) throws IOException {

        int minInputCount = 4;
        int maxInputCount = 7;
        int runCount = 500;

        File outputDir = new File("TestDataGenerator/testdata");
        if (outputDir.isDirectory()) {
            FileUtils.deleteDirectory(outputDir);
        }
        outputDir.mkdirs();

        for(int runNo = 0; runNo < runCount; runNo++) {

            boolean minimizationTarget = rng.nextBoolean();
            int inputCount = rng.nextInt(maxInputCount - minInputCount) + minInputCount;
            int rowCount = (int) Math.pow(2, inputCount);

            try(FileWriter writer = new FileWriter(new File(outputDir, "test" + runNo), true)) {
                writer.write("target " + (minimizationTarget ? '1' : '0') + '\n');
                for(int rowNo = 0; rowNo < rowCount; rowNo++) {

                    StringBuilder rowSb = new StringBuilder();

                    List<Boolean> inputs = BooleanUtils.getPermutation(rowNo, inputCount);

                    Boolean output = rng.nextBoolean();

                    inputs.stream().forEach(b -> rowSb.append(b ? '1' : '0'));
                    rowSb.append(' ').append(output ? '1' : '0').append('\n');

                    writer.write(rowSb.toString());
                }

            }
        }
    }
}
