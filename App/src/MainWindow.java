import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class MainWindow {
    public MainWindow() throws IOException, URISyntaxException {
        JFrame frame = new JFrame();
        frame.setTitle(Strings.get("main-window-title"));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setMinimumSize(new Dimension(800, 400));

        MainTabbedPane mainTabbedPane = new MainTabbedPane();
        frame.add(mainTabbedPane.j());

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
