import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseListener;

public class LatexExpressionView {

    public LatexExpressionView() {
        LatexLabel latexLabel = LatexLabel.blankHeightKeeper();
        latexLabel.setBorder(createLabelBorder());
        j = new JScrollPane(latexLabel);
        j.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    }

    public JScrollPane j() {
        return j;
    }

    public void setModel(String expressionText) {
        LatexLabel l = new LatexLabel(expressionText);
        l.setBorder(createLabelBorder());
        j.setViewportView(l);

        JScrollBar scrollbar = j.getHorizontalScrollBar();
        scrollbar.setValue(scrollbar.getMaximum());
    }

    public void redraw() {
    }

    private EmptyBorder createLabelBorder() {
        return new EmptyBorder(0, 0, getScrollbarThickness(), 0);
    }

    private Integer getScrollbarThickness()  {
        return (Integer)UIManager.get("ScrollBar.width");
    }

    public void addMouseListener(MouseListener listener) {
        j.addMouseListener(listener);
    }

    private final JScrollPane j;

    public void resetScroll() {
        j.getHorizontalScrollBar().setValue(0);
    }
}
