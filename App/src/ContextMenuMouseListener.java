import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class ContextMenuMouseListener extends MouseAdapter {

    protected ContextMenuMouseListener(List<XSupplier<JMenuItem>> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (!e.isPopupTrigger()) {
            return;
        }

        JPopupMenu contextMenu = new JPopupMenu();
        for(XSupplier<JMenuItem> item : menuItems) {
            contextMenu.add(item.get());
        }

        contextMenu.show(e.getComponent(), e.getX(), e.getY());
    }

    private final List<XSupplier<JMenuItem>> menuItems;
}

