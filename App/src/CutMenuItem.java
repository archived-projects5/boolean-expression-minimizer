import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import java.io.IOException;
import java.net.URISyntaxException;

public class CutMenuItem extends JMenuItem {
    public CutMenuItem(JTextComponent textArea) throws IOException, URISyntaxException {
        super(Strings.get("cut"), Icons.get("cut"));

        setEnabled(!StringUtils.isNullOrEmpty(textArea.getSelectedText()));

        addActionListener(new DefaultEditorKit.CutAction());
    }
}
