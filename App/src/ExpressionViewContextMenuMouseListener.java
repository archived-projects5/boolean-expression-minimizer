import java.util.Arrays;
import java.util.function.Supplier;

public class ExpressionViewContextMenuMouseListener extends ContextMenuMouseListener {

    public ExpressionViewContextMenuMouseListener(
            Supplier<Boolean> canExportExpression,
            Supplier<String> getTxtExpressionText,
            Supplier<String> getXmlExpressionText) {

        super(Arrays.asList(
                () -> new ExportMenuItem(
                        Strings.get("export-as-txt"),
                        Icons.get("export-as-txt"),
                        canExportExpression,
                        getTxtExpressionText),

                () -> new ExportMenuItem(
                        Strings.get("export-as-xml"),
                        Icons.get("export-as-xml"),
                        canExportExpression,
                        getXmlExpressionText)
        ));
    }
}
