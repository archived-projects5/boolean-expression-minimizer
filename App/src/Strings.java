import java.io.InputStream;
import java.util.PropertyResourceBundle;

public class Strings {

    public static String get(String key) {
        try (InputStream istream = Strings.class.getResourceAsStream( "res/strings.res")) {
            PropertyResourceBundle propertyBundle = new PropertyResourceBundle(istream);
            return propertyBundle.getString(key);
        } catch(Exception ex) {
            ex.printStackTrace();
            Errors.dumpToFile(ex);
            return "?";
        }
    }
}
