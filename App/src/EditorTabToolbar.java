import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.function.Consumer;

public class EditorTabToolbar {

    private final Consumer<IMinimizeResult> minimizeSuccessHandler;

    public EditorTabToolbar(Consumer<IMinimizeResult> minimizeSuccessHandler, Consumer<Boolean> enableTextArea) throws IOException, URISyntaxException {
        this.minimizeSuccessHandler = minimizeSuccessHandler;
        this.enableTextArea = enableTextArea;
        j = new JPanel();

        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        j.setLayout(layout);

        JLabel algorithmSelectionLabel = new JLabel(Strings.get("select-algorithm"));
        j.add(algorithmSelectionLabel);

        algorithmSelectionComboBox = new JComboBox<>();

        algorithmSelectionComboBox.addItem(new ComboBoxItem<>(
                new EspressoMinimizerAlgorithm(),
                Strings.get("espresso-combobox-item")));

        algorithmSelectionComboBox.addItem(new ComboBoxItem<>(
                new QuineMcCluskeyMinimizerAlgorithm(),
                Strings.get("quine-mccluskey-combobox-item")));

        j.add(algorithmSelectionComboBox);

        JLabel minimizeForLabel = new JLabel(Strings.get("result-form"));
        j.add(minimizeForLabel);

        targetOutputValueSelectionComboBox = new JComboBox<>();
        targetOutputValueSelectionComboBox.addItem(new ComboBoxItem<>(true, Strings.get("sum-of-products")));
        targetOutputValueSelectionComboBox.addItem(new ComboBoxItem<>(false, Strings.get("product-of-sums")));
        j.add(targetOutputValueSelectionComboBox);

        startMinimizeButton = new JButton(
                Strings.get("start-minimize"), Icons.get("start-minimize"));
        startMinimizeButton.setEnabled(false);
        j.add(startMinimizeButton);

        cancelMinimizeButton = new JButton(
                Strings.get("cancel-minimize"), Icons.get("cancel-minimize"));
        cancelMinimizeButton.setEnabled(false);
        j.add(cancelMinimizeButton);

        startMinimizeButton.addActionListener(actionEvent -> {
            setGuiStateMinimizationStarted();
            minimizeTask = createMinimizeBackgroundTask();
            minimizeTask.execute();
        });

        cancelMinimizeButton.addActionListener(actionEvent -> {
            minimizeTask.cancel(true);
            setGuiStateMinimizationEnded();
        });
    }

    public void setParsedExpression(IParsedExpression parsedExpr) {
        latestParsedExpression = parsedExpr;
    }

    public void setNextMinimizeButtonState(boolean enabled) {
        if (minimizeTask == null || minimizeTask.isDone()) {
            startMinimizeButton.setEnabled(enabled);
            nextMinimizeEnableState = Optional.of(enabled);
        } else {
            nextMinimizeEnableState = Optional.of(enabled);
        }
    }

    public JPanel j() {
        return j;
    }

    private IMinimizerAlgorithm getSelectedAlgorithm() {
        return ((ComboBoxItem<IMinimizerAlgorithm>) algorithmSelectionComboBox.getSelectedItem()).getStoredValue();
    }

    private void minimizeCancelHandler() {
        destroyEspressoProcess();
    }

    private void minimizeErrorHandler() {
        destroyEspressoProcess();
    }

    private void minimizeAnyEndHandler() {
        setGuiStateMinimizationEnded();
        ifEspresso(esp -> esp.cleanup());
    }

    private void ifEspresso(Consumer<EspressoMinimizerAlgorithm> process) {
        IMinimizerAlgorithm selectedAlgorithm = getSelectedAlgorithm();
        if (selectedAlgorithm instanceof EspressoMinimizerAlgorithm) {
            process.accept((EspressoMinimizerAlgorithm)selectedAlgorithm);
        }
    }

    private void destroyEspressoProcess() {
        ifEspresso(esp -> esp.destroyProcess());
    }

    private boolean getSelectedTargetOutput() {
        return ((ComboBoxItem<Boolean>)targetOutputValueSelectionComboBox.getSelectedItem()).getStoredValue();
    }

    private SwingWorker<IMinimizeResult, Void> createMinimizeBackgroundTask() {
        return new SwingWorker<IMinimizeResult, Void>() {
            @Override
            protected IMinimizeResult doInBackground() throws
                    UnsupportedPlatformException,
                    InterruptedException,
                    IOException,
                    URISyntaxException {
                IMinimizerAlgorithm selectedAlgorithm = getSelectedAlgorithm();
                boolean selectedTargetOutput = getSelectedTargetOutput();
                TruthTable inputTruthTable = latestParsedExpression.evaluate();
                return selectedAlgorithm.run(inputTruthTable, selectedTargetOutput);
            }

            @Override
            protected void done() {
                try {
                    IMinimizeResult result = get();
                    minimizeSuccessHandler.accept(result);
                } catch (CancellationException | InterruptedException e) {
                    minimizeCancelHandler();
                } catch (Exception e) {
                    this.cancel(false);
                    minimizeErrorHandler();
                    Errors.dumpToFile(e);
                    Errors.showDialog(Strings.get("espresso-error"), e);
                } finally {
                    minimizeAnyEndHandler();
                }
            }
        };
    }

    private void setGuiStateMinimizationStarted() {
        startMinimizeButton.setEnabled(false);
        cancelMinimizeButton.setEnabled(true);
        algorithmSelectionComboBox.setEnabled(false);
        targetOutputValueSelectionComboBox.setEnabled(false);
        enableTextArea.accept(false);

    }

    private final Consumer<Boolean> enableTextArea;

    private void setGuiStateMinimizationEnded() {
        startMinimizeButton.setEnabled(nextMinimizeEnableState.get());
        cancelMinimizeButton.setEnabled(false);
        algorithmSelectionComboBox.setEnabled(true);
        targetOutputValueSelectionComboBox.setEnabled(true);
        enableTextArea.accept(true);
    }

    private final JPanel j;
    private final JButton startMinimizeButton;
    private final JButton cancelMinimizeButton;
    private final JComboBox<ComboBoxItem<IMinimizerAlgorithm>> algorithmSelectionComboBox;
    private final JComboBox<ComboBoxItem<Boolean>> targetOutputValueSelectionComboBox;

    private SwingWorker<IMinimizeResult, Void> minimizeTask;
    private Optional<Boolean> nextMinimizeEnableState = Optional.empty();

    private IParsedExpression latestParsedExpression;

}
