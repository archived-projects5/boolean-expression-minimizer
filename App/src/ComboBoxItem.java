
public class ComboBoxItem<T> {
    public ComboBoxItem(T storedValue, String label) {
        this.storedValue = storedValue;
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }

    public T getStoredValue() {
        return storedValue;
    }

    private final T storedValue;
    private final String label;
}
