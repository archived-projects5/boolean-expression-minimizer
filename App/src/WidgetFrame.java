import javax.swing.*;
import java.awt.*;

public class WidgetFrame {

    public WidgetFrame(String title, JComponent content) {
        j = new JPanel(new BorderLayout());

        JPanel header = new JPanel(new FlowLayout(FlowLayout.LEFT));
        header.add(new JLabel(title));

        j.add(header, BorderLayout.PAGE_START);
        j.add(content, BorderLayout.CENTER);
    }

    public JPanel j() {
        return j;
    }

    private final JPanel j;
}
