import javax.swing.text.JTextComponent;
import java.util.Arrays;

public class ReadonlyTextAreaContextMenuMouseListener extends ContextMenuMouseListener {
    public ReadonlyTextAreaContextMenuMouseListener(JTextComponent textArea) {
        super(Arrays.asList(
                () -> new CopyAllMenuItem(textArea),
                () -> new CopyMenuItem(textArea)
        ));
    }
}
