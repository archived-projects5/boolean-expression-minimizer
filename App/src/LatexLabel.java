import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;

import javax.swing.*;

public class LatexLabel extends JLabel {
    public LatexLabel(String latexText) {
        super(makeLatexIcon(latexText));
    }

    public static LatexLabel blankHeightKeeper() {
        return new LatexLabel("");
    }

    private static Icon makeLatexIcon(String latexText) {
        String blankHeightStabilizer = "\\vphantom{\\prod \\sum (I)}";
        TeXFormula texFormula = new TeXFormula(blankHeightStabilizer + latexText);
        return texFormula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 30);
    }
}
