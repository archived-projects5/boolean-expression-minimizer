import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;
import java.util.Arrays;

public class EditorTextAreaContextMenuListener extends ContextMenuMouseListener {
    public EditorTextAreaContextMenuListener(JTextComponent textArea, UndoManager undoManager) {
        super(Arrays.asList(
            () -> new CutMenuItem(textArea),
            () -> new CopyAllMenuItem(textArea),
            () -> new CopyMenuItem(textArea),
            () -> new PasteMenuItem(),
            () -> new UndoMenuItem(undoManager),
            () -> new RedoMenuItem(undoManager)
        ));
    }
}
