public class MarkupExpressionSymbols extends ExpressionSymbols {
    public MarkupExpressionSymbols() {
        super(
                "<index>",
                "</index>",
                "<single-neg>",
                "</single-neg>",
                "<multi-neg>",
                "</multi-neg>",
                "<xor/>",
                "<and/>",
                "<or/>",
                "<paren>",
                "</paren>",
                "<id>",
                "</id>",
                "<one/>",
                "<zero/>"
        );
    }
}
