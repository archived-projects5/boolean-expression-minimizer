import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class ResultsTab {

    private final MainTabbedPane parentTabbedPane;
    private final ViewBox expressionViewBox = new ViewBox();
    private final ViewBox truthTableViewBox = new ViewBox();

    public ResultsTab(MainTabbedPane parentTabbedPane) throws IOException, URISyntaxException {
        this.parentTabbedPane = parentTabbedPane;
        j = new JPanel(new BorderLayout());

        WidgetFrame framedExpressionView = new WidgetFrame(Strings.get("expression-view"), expressionViewBox.j());
        j.add(framedExpressionView.j(), BorderLayout.PAGE_START);

        WidgetFrame framedTruthTableView = new WidgetFrame(Strings.get("truth-table-view"), truthTableViewBox.j());
        j.add(framedTruthTableView.j(), BorderLayout.CENTER);

    }

    public JPanel j() {
        return j;
    }

    public void setModel(IMinimizeResult result) throws IOException, URISyntaxException {
        SymbolicExpressionView expressionView = new SymbolicExpressionView(result.getExpression());
        expressionViewBox.set(expressionView.j());

        TruthTableView truthTableView = new TruthTableView(result.getTruthTable());
        truthTableViewBox.set(truthTableView.j());
    }

    public void redraw() {
    }

    public boolean isEnabled() {
        return parentTabbedPane.isResultsTabEnabled();
    }

    public void enable() {
        parentTabbedPane.enableResultsTab();
    }

    public void open() {
        parentTabbedPane.openResultsTab();
    }

    public boolean isOpen() {
        return parentTabbedPane.isResultsTabOpen();
    }

    private final JPanel j;
}
