import javax.swing.*;
import javax.swing.undo.UndoManager;
import java.io.IOException;
import java.net.URISyntaxException;

public class UndoMenuItem extends JMenuItem {
    public UndoMenuItem(UndoManager undoManager) throws IOException, URISyntaxException {
        super(Strings.get("undo"), Icons.get("undo"));
        setEnabled(undoManager.canUndo());
        addActionListener(action -> undoManager.undo());
    }
}
