import javax.swing.*;

public class RawStringExpressionView {

    public RawStringExpressionView(String expressionText) {
        JTextArea textArea = new JTextArea();
        textArea.setFont(textArea.getFont().deriveFont(textArea.getFont().getSize2D()*1.5f));
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setText(expressionText);
        textArea.addMouseListener(new ReadonlyTextAreaContextMenuMouseListener(textArea));

        j = new JScrollPane(textArea);
    }

    public JScrollPane j() {
        return j;
    }

    private final JScrollPane j;
}
