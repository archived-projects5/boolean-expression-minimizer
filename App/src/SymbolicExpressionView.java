import javax.swing.*;

public class SymbolicExpressionView {

    static class DataFormats {
        String txt;
        String xml;
        String latex;
    }

    public SymbolicExpressionView(SymbolicExpression model) {
        DataFormats expressionFormats = stringifyModel(model);

        innerView.setModel(expressionFormats.latex);
        innerView.resetScroll();
        innerView.addMouseListener(new ExpressionViewContextMenuMouseListener(
                () -> true,
                () -> expressionFormats.txt,
                () -> expressionFormats.xml));
    }

    public JComponent j() {
        return innerView.j();
    }

    private void stringifyVariable(
            SymbolicExpression.Term.Variable variable,
            ExpressionSymbols symbols,
            StringBuilder sb) {
        if (variable.negated) {
            sb.append(symbols.singleNegationBegin);
        }

        sb.append(symbols.idBegin);
        sb.append(variable.name.charAt(0));
        sb.append(symbols.idEnd);

        if (variable.name.length() > 1) {
            sb.append(symbols.indexBegin);
            sb.append(variable.name.substring(1));
            sb.append(symbols.indexEnd);
        }

        if (variable.negated) {
            sb.append(symbols.singleNegationEnd);
        }
    }

    private DataFormats stringifyModel(SymbolicExpression model) {
        LatexExpressionSymbols latexSymbols = new LatexExpressionSymbols();
        MarkupExpressionSymbols xmlSymbols = new MarkupExpressionSymbols();
        TxtExpressionSymbols txtSymbols = new TxtExpressionSymbols();

        if (model.specialValue != null) {
            if (model.specialValue == SymbolicExpression.SpecialType.One) {
                return new DataFormats() {{
                    latex = latexSymbols.one;
                    txt = txtSymbols.one;
                    xml = xmlSymbols.one;
                }};
            } else {
                return new DataFormats() {{
                    latex = latexSymbols.zero;
                    txt = txtSymbols.zero;
                    xml = xmlSymbols.zero;
                }};
            }
        }

        StringBuilder xmlSb = new StringBuilder();
        StringBuilder txtSb = new StringBuilder();
        StringBuilder latexSb = new StringBuilder();

        model.terms.forEach(term -> {

            if (model.type == SymbolicExpression.Type.Conjunctive
                    && model.terms.size() > 1
                    && term.variables.size() > 1) {

                latexSb.append(latexSymbols.parenBegin);
                txtSb.append(txtSymbols.parenBegin);
                xmlSb.append(xmlSymbols.parenEnd);

            }

            term.variables.forEach(variable -> {
                stringifyVariable(variable, latexSymbols, latexSb);
                stringifyVariable(variable, xmlSymbols, xmlSb);
                stringifyVariable(variable, txtSymbols, txtSb);

                if (model.type == SymbolicExpression.Type.Conjunctive) {
                    latexSb.append(latexSymbols.or);
                    xmlSb.append(xmlSymbols.or);
                    txtSb.append(txtSymbols.or);
                } else {
                    latexSb.append(latexSymbols.and);
                    xmlSb.append(xmlSymbols.and);
                    txtSb.append(txtSymbols.and);
                }
            });

            if (model.type == SymbolicExpression.Type.Conjunctive) {
                latexSb.delete(latexSb.length() - latexSymbols.or.length(), latexSb.length());
                xmlSb.delete(xmlSb.length() - xmlSymbols.or.length(), xmlSb.length());
                txtSb.delete(txtSb.length() - txtSymbols.or.length(), txtSb.length());

                if (term.variables.size() > 1 && model.terms.size() > 1) {
                    latexSb.append(latexSymbols.parenEnd);
                    xmlSb.append(xmlSymbols.parenEnd);
                    txtSb.append(txtSymbols.parenEnd);
                }

                latexSb.append(latexSymbols.and);
                xmlSb.append(xmlSymbols.and);
                txtSb.append(txtSymbols.and);

            } else {
                latexSb.delete(latexSb.length() - latexSymbols.and.length(), latexSb.length());
                xmlSb.delete(xmlSb.length() - xmlSymbols.and.length(), xmlSb.length());
                txtSb.delete(txtSb.length() - txtSymbols.and.length(), txtSb.length());

                latexSb.append(latexSymbols.or);
                xmlSb.append(xmlSymbols.or);
                txtSb.append(txtSymbols.or);
            }
        });

        if (model.type == SymbolicExpression.Type.Disjunctive) {
            latexSb.delete(latexSb.length() - latexSymbols.or.length(), latexSb.length());
            xmlSb.delete(xmlSb.length() - xmlSymbols.or.length(), xmlSb.length());
            txtSb.delete(txtSb.length() - txtSymbols.or.length(), txtSb.length());
        } else {
            latexSb.delete(latexSb.length() - latexSymbols.and.length(), latexSb.length());
            xmlSb.delete(xmlSb.length() - xmlSymbols.and.length(), xmlSb.length());
            txtSb.delete(txtSb.length() - txtSymbols.and.length(), txtSb.length());
        }

        return new DataFormats() {{
            latex = latexSb.toString();
            xml = xmlSb.toString();
            txt = txtSb.toString();
        }};
    }

    private final LatexExpressionView innerView = new LatexExpressionView();
}
