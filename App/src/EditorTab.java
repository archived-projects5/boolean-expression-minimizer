import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.swing.*;
import java.util.List;

public class EditorTab {
    public EditorTab(Consumer<IMinimizeResult> onMinimizeSuccess) throws IOException, URISyntaxException {
        j = new JPanel();
        j.setLayout(new BorderLayout());

        JPanel previewAndEditor = new JPanel();
        j.add(previewAndEditor, BorderLayout.CENTER);
        previewAndEditor.setLayout(new BorderLayout());

        EditorExpressionPreview preview = new EditorExpressionPreview();
        previewAndEditor.add(preview.j(), BorderLayout.PAGE_START);

        EditorTextArea editor = new EditorTextArea();
        previewAndEditor.add(editor.j(), BorderLayout.CENTER);

        EditorTabToolbar toolbar = new EditorTabToolbar(onMinimizeSuccess, enable -> editor.enableTextArea(enable));
        j.add(toolbar.j(), BorderLayout.PAGE_START);

        editor.addChangeListener(newText -> {
            lastParsedExpression = parseExpression(newText);

            editor.setErrors(() ->
                    getLastParsedExpression().getErrors()
                            .stream()
                            .map(coords -> calculateErrorIndex(coords, newText))
                            .collect(Collectors.toList()));

            toolbar.setNextMinimizeButtonState(lastParsedExpression.canBeEvaluated());
            toolbar.setParsedExpression(lastParsedExpression);
            preview.setModel(lastParsedExpression);
            j.revalidate();
            j.repaint();
        });
    }

    private IParsedExpression getLastParsedExpression() {
        return lastParsedExpression;
    }

    public JPanel j() {
        return j;
    }

    private int calculateErrorIndex(CharCoords coords, String text) {
        int idx = 0;
        String[] lines = text.split(System.lineSeparator(), -1);
        for(int i = 0; i < coords.line; i++) {
            idx += lines[i].length() + 1;
        }
        idx += coords.column;
        return idx;
    }

    private IParsedExpression parseExpression(String expr) {
        return new AntlrBooleanExpressionParser().parse(expr);
    }

    private final JPanel j;
    private IParsedExpression lastParsedExpression;
}

