import java.util.function.Supplier;

@FunctionalInterface
public interface XSupplier<T> extends Supplier<T> {

    @Override
    default T get() {
        try {
            return getThrows();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    T getThrows() throws Exception;

}
