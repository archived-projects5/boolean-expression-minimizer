import javax.swing.*;


public class EditorExpressionPreview {

    public EditorExpressionPreview() {

        ExpressionViewContextMenuMouseListener contextMenuListener = new ExpressionViewContextMenuMouseListener(
                this::canExportExpression,
                this::getExpressionAsTxt,
                this::getExpressionAsXml);

        expressionTextView.addMouseListener(contextMenuListener);
    }

    private String getExpressionAsTxt() {
        return model.getText(new TxtExpressionSymbols());
    }

    private String getExpressionAsXml() {
        return model.getText(new MarkupExpressionSymbols());
    }

    public JComponent j() {
        return expressionTextView.j();
    }

    public void setModel(IParsedExpression model) {
        this.model = model;
        expressionTextView.setModel(getExpressionText(new LatexExpressionSymbols()));
    }

    public void redraw() {
        expressionTextView.redraw();
    }

    private String getExpressionText(ExpressionSymbols symbols) {
        return model != null ? model.getText(symbols) : "";
    }

    private boolean canExportExpression() {
        return model != null && model.canBeEvaluated();
    }

    private IParsedExpression model = null;

    private final LatexExpressionView expressionTextView = new LatexExpressionView();
}

