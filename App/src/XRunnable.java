@FunctionalInterface
public interface XRunnable extends Runnable {

    @Override
    default void run() {
        try {
            runThrows();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void runThrows() throws Exception;

}
