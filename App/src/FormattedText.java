public class FormattedText {

    public final String formatName;
    public final String text;

    public FormattedText(String formatName, String text) {
        this.formatName = formatName;
        this.text = text;
    }
}
