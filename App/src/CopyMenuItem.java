import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import java.io.IOException;
import java.net.URISyntaxException;

public class CopyMenuItem extends JMenuItem {
    public CopyMenuItem(JTextComponent textArea) throws IOException, URISyntaxException {
        super(Strings.get("copy"), Icons.get("copy"));

        setEnabled(!StringUtils.isNullOrEmpty(textArea.getSelectedText()));

        addActionListener(new DefaultEditorKit.CopyAction());
    }
}
