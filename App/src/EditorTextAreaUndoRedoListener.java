import javax.swing.*;
import javax.swing.undo.UndoManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class EditorTextAreaUndoRedoListener extends KeyAdapter {
    public EditorTextAreaUndoRedoListener(UndoManager undoManager) {
        this.undoManager = undoManager;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        canUndoRedo = true;
    }

    private boolean canUndoRedo = true;

    @Override
    public void keyPressed(KeyEvent event) {
        if (isCtrlZee(event) && undoManager.canUndo() && canUndoRedo) {
            undoManager.undo();
//            canUndoRedo = false;
        } else if (isCtrlWye(event) && undoManager.canRedo() && canUndoRedo) {
            undoManager.redo();
//            canUndoRedo = false;
        }
    }

    private boolean isCtrlZee(KeyEvent event) {
        return event.isControlDown() && event.getKeyCode() == 65 + 'z' - 'a';
    }

    private boolean isCtrlWye(KeyEvent event) {
        return event.isControlDown() && event.getKeyCode() == 65 + 'y' - 'a';
    }

    private UndoManager undoManager;
}
