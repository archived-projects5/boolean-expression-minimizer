import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.net.URISyntaxException;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class TruthTableView {

    public JScrollPane j() {
        return j;
    }

    private final JScrollPane j;
    private final JTable jtable;
    private final TruthTable model;

    public TruthTableView(TruthTable truthTable) throws IOException, URISyntaxException {
        model = truthTable;
        jtable = makeJTable(truthTable);
        j = new JScrollPane(jtable) {{
            setBorder(null);
        }};

        TruthTableViewContextMenuMouseListener contextMenuListener = new TruthTableViewContextMenuMouseListener(
                this::makeCsvString,
                this::makeXmlString);
        jtable.addMouseListener(contextMenuListener);
        jtable.getTableHeader().addMouseListener(contextMenuListener);
    }

    private String makeCsvString() {
        StringBuilder sb = new StringBuilder();

        model.getHeaderRow().forEach(h -> sb.append(h + ","));
        sb.append("Output");
        sb.append("\n");

        model.getData().forEach(row -> {
            row.getInputs().forEach(i -> sb.append(i.toChar()).append(","));
            sb.append(row.getOutput().toChar()).append("\n");
        });

        return sb.toString();
    }

    private String makeXmlString() {

        StringBuilder sb = new StringBuilder();

        sb.append("<head>");
        sb.append("<h-row>");

        model.getHeaderRow().forEach(h -> {
            sb.append("<h-cell>");
            sb.append(h);
            sb.append("</h-cell>");
        });
        sb.append("<h-cell>");
        sb.append("Output");
        sb.append("</h-cell>");

        sb.append("</h-row>");
        sb.append("</head>");

        sb.append("<body>");
        model.getData().forEach(row -> {
            sb.append("<b-row>");
            row.getInputs().forEach(i -> {
                sb.append("<b-cell>");
                sb.append(i.toChar());
                sb.append("</b-cell>");
            });
            sb.append("<b-cell>").append(row.getOutput().toChar()).append("</b-cell>");
            sb.append("</b-row>");
        });
        sb.append("</body>");

        return sb.toString();
    }


    private JTable makeJTable(TruthTable truthTable) throws IOException, URISyntaxException {

        String[] headerRow = new String[truthTable.getHeaderRow().size() + 1];
        headerRow[headerRow.length - 1] = Strings.get("output");
        IntStream.range(0, headerRow.length - 1).forEach(i -> {
            headerRow[i] = truthTable.getHeaderRow().get(i);
        });

        ArrayList<Object[]> dataRows = new ArrayList<Object[]>();
        truthTable.getData().forEach(row -> {
            ArrayList<Object> dataRow = new ArrayList<>();
            dataRow.addAll(row.getInputs().stream().map(TriState::toChar).collect(Collectors.toList()));
            dataRow.add(row.getOutput().toChar());
            dataRows.add(dataRow.toArray());
        });

        JTable jtable = new JTable(dataRows.toArray(new Object[0][0]), headerRow) {
            {
                centerDataInCells(this);
                adjustTableHeightToMatchRowCount(this);
                setShowVerticalLines(true);
                setShowHorizontalLines(true);
                setRowSelectionAllowed(false);
                setCellSelectionEnabled(false);
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        return jtable;
    }

    private void centerDataInCells(JTable table) {
        DefaultTableCellRenderer tableCellRenderer = ((DefaultTableCellRenderer) table.getDefaultRenderer(Object.class));
        tableCellRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
    }

    private void adjustTableHeightToMatchRowCount(JTable table) {
        Dimension preferredViewportSize = new Dimension(
                table.getPreferredSize().width,
                table.getRowHeight() * table.getRowCount());
        table.setPreferredScrollableViewportSize(preferredViewportSize);
    }
}
