public class LatexExpressionSymbols extends ExpressionSymbols {
    public LatexExpressionSymbols() {
        super(
                "_{",
                "}",
                "\\overline{",
                "}",
                "\\overline{",
                "}",
                "\\oplus ",
                "\\hspace{0.15em}",
                "+",
                "(",
                ")",
                "",
                "",
                "1",
                "0"
        );
    }
}
