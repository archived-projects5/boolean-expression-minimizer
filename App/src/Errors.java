import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Errors {
    public static void showDialog(String title, Exception exception) {
        String dialogMessage = exception.getMessage();
        dialogMessage += "\n" + Strings.get("see-stacktrace");
        JOptionPane.showMessageDialog(null, dialogMessage, title, JOptionPane.ERROR_MESSAGE);
    }

    public static void dumpToFile(Exception exception) {
        try (PrintStream ps = new PrintStream("stacktrace.log")) {
            exception.printStackTrace(ps);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
