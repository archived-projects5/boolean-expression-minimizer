import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.text.*;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class EditorTextArea {
    public EditorTextArea() {
        undoManager = new UndoManager();
        textPane = new JTextPane();
        textPane.setFont(textPane.getFont().deriveFont(textPane.getFont().getSize2D()*1.5f));
        textPane.getDocument().addUndoableEditListener(undoManager);
        textPane.addKeyListener(new EditorTextAreaUndoRedoListener(undoManager));
        textPane.addMouseListener(new EditorTextAreaContextMenuListener(textPane, undoManager));
        originalTextAttributes = new SimpleAttributeSet(textPane.getCharacterAttributes());
        j = new JScrollPane(textPane);
    }

    public void setErrors(Supplier<List<Integer>> indices) {
        textPane.getHighlighter().removeAllHighlights();
        indices.get().forEach(i -> {
            try {
                textPane.getHighlighter().addHighlight(
                        i, i + 1, new RedBackgroundHighlighterPainter());
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        });
    }

    public void addChangeListener(Consumer<String> listener) {
        textPane.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                listener.accept(textPane.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                listener.accept(textPane.getText());
            }
        });

    }

    public JScrollPane j() {
        return j;
    }

    public void redraw() {
    }

    private final JScrollPane j;
    private final JTextPane textPane;
    private final UndoManager undoManager;
    private final AttributeSet originalTextAttributes;

    public void enableTextArea(Boolean enable) {
        textPane.setEditable(enable);
    }
}

