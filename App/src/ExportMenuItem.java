import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.function.Supplier;


public class ExportMenuItem extends JMenuItem {
    public ExportMenuItem(
            String text,
            Icon icon,
            Supplier<Boolean> enable,
            Supplier<String> getTextData) {

        super(text, icon);
        setEnabled(enable.get());
        addActionListener(action -> {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(new StringSelection(getTextData.get()), null);
        });
    }
}

