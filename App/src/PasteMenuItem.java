import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import java.io.IOException;
import java.net.URISyntaxException;

public class PasteMenuItem extends JMenuItem {
    public PasteMenuItem() throws IOException, URISyntaxException {
        super(Strings.get("paste"), Icons.get("paste"));
        addActionListener(new DefaultEditorKit.PasteAction());
    }
}
