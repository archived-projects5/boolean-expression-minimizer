import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.net.URISyntaxException;

public class CopyAllMenuItem extends JMenuItem {
    public CopyAllMenuItem(JTextComponent textArea) throws IOException, URISyntaxException {
        super(Strings.get("copy-all"), Icons.get("copy-all"));

        addActionListener(action -> {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            String text = textArea.getText() != null ? textArea.getText() : "";
            clipboard.setContents(new StringSelection(text), null);
        });
    }
}
