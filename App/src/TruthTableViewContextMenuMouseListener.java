import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class TruthTableViewContextMenuMouseListener extends ContextMenuMouseListener {
    public TruthTableViewContextMenuMouseListener(
            Supplier<String> getCsvString,
            Supplier<String> getXmlString
    )  {
        super(Arrays.asList(
                () -> new ExportMenuItem(
                        Strings.get("export-as-csv"),
                        Icons.get("export-as-csv"),
                        () -> true,
                        getCsvString),

                () -> new ExportMenuItem(
                        Strings.get("export-as-xml"),
                        Icons.get("export-as-xml"),
                        () -> true,
                        getXmlString)
        ));
    }
}
