import javax.swing.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class MainTabbedPane {

    public MainTabbedPane() throws IOException, URISyntaxException {
        j = new JTabbedPane();

        EditorTab editorTab = new EditorTab(this::onMinimizeSuccess);
        j.add(Strings.get("editor-tab-title"), editorTab.j());

        resultsTab = new ResultsTab(this);
        j.add(Strings.get("results-tab-title"), resultsTab.j());
        j.setEnabledAt(1, false);
    }

    public JTabbedPane j() {
        return j;
    }

    public void openResultsTab() {
        j.setSelectedIndex(1);
    }

    public boolean isResultsTabOpen() {
        return j.getSelectedIndex() == 1;
    }

    public void enableResultsTab() {
        j.setEnabledAt(1, true);
    }

    public boolean isResultsTabEnabled() {
        return j.isEnabledAt(1);
    }

    private void onMinimizeSuccess(IMinimizeResult result) {
        try {
            resultsTab.setModel(result);
            resultsTab.redraw();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        if (!resultsTab.isEnabled()) {
            resultsTab.enable();
            resultsTab.open();
        } else if (!resultsTab.isOpen()) {
            resultsTab.open();
        }
    }

    private final JTabbedPane j;
    private final ResultsTab resultsTab;

}
