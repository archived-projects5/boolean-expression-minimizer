import javax.swing.*;
import java.awt.*;

public class ViewBox {
    public void set(JComponent content) {
        if (j.getComponentCount() > 0) {
            j.removeAll();
        }
        j.add(content, BorderLayout.CENTER);
    }

    public JPanel j() {
        return j;
    }

    private final JPanel j = new JPanel(new BorderLayout());
}

