import com.formdev.flatlaf.FlatLightLaf;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Program {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                FlatLightLaf.setup();
                new MainWindow();
            } catch (Exception e) {
                e.printStackTrace();
                Errors.dumpToFile(e);
                Errors.showDialog(Strings.get("fatal-error"), e);
            }
        });
    }
}
