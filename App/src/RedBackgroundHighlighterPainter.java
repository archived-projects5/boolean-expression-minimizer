import javax.swing.text.DefaultHighlighter;
import java.awt.*;

public class RedBackgroundHighlighterPainter extends DefaultHighlighter.DefaultHighlightPainter {
    public RedBackgroundHighlighterPainter() {
        super(Color.getHSBColor(0f, 0.698f, 1f));
    }
}
