import javax.swing.*;

public class StringUtils {
    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0;
    }
}
