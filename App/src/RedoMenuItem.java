import javax.swing.*;
import javax.swing.undo.UndoManager;
import java.io.IOException;
import java.net.URISyntaxException;

public class RedoMenuItem extends JMenuItem {
    public RedoMenuItem(UndoManager undoManager) throws IOException, URISyntaxException {
        super(Strings.get("redo"), Icons.get("redo"));
        setEnabled(undoManager.canRedo());
        addActionListener(action -> undoManager.redo());
    }
}
