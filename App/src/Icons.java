import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.io.InputStream;

public class Icons {

    public static Icon get(String key) {
        if (key.equals("error-dialog")) {
            return UIManager.getIcon("OptionPane.errorIcon");
        }
        if (key.equals("errors-in-expression")) {
            return UIManager.getIcon("OptionPane.warningIcon");
        }
        try (InputStream istream = Icons.class.getResourceAsStream("res/icons/" + key + ".png")) {
             return new ImageIcon(IOUtils.toByteArray(istream));
        } catch (Exception ex) {
            ex.printStackTrace();
            Errors.dumpToFile(ex);
            return null;
        }
    }

}
